module.exports = [
  {
    type: 'tree',
    icon: 'fa fa-circle-o',
    name: 'SW1',
    items: [
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW1.1',
        router: {
          name: 'SemestralWork1'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW1.2',
        router: {
          name: 'SemestralWork12'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW1.3',
        router: {
          name: 'SemestralWork13'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW1.4',
        router: {
          name: 'SemestralWork14'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW1.5',
        router: {
          name: 'SemestralWork15'
        }
      }
    ]
  },
  {
    type: 'tree',
    icon: 'fa fa-circle-o',
    name: 'SW2',
    items: [
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW2.1',
        router: {
          name: 'SemestralWork2'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW2.2',
        router: {
          name: 'SemestralWork22'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW2.3',
        router: {
          name: 'SemestralWork23'
        }
      },
      {
        type: 'item',
        icon: 'fa fa-circle-o',
        name: 'SW2.4',
        router: {
          name: 'SemestralWork24'
        }
      },
    ]
  },
  {
    type: 'item',
    icon: 'fa fa-circle-o',
    name: 'SW3',
    router: {
      name: 'SemestralWork3'
    }
  },
  {
    type: 'item',
    icon: 'fa fa-circle-o',
    name: 'SW4',
    router: {
      name: 'SemestralWork4'
    }
  }
]
