import Vue from 'vue'
import Router from 'vue-router'

import SemestralWork1 from '../components/SemestralWork/SemestralWork1.vue'
import SemestralWork12 from '../components/SemestralWork/SemestralWork12.vue'
import SemestralWork13 from '../components/SemestralWork/SemestralWork13.vue'
import SemestralWork14 from '../components/SemestralWork/SemestralWork14.vue'
import SemestralWork15 from '../components/SemestralWork/SemestralWork15.vue'
import SemestralWork2 from '../components/SemestralWork/SemestralWork2.vue'
import SemestralWork22 from '../components/SemestralWork/SemestralWork22.vue'
import SemestralWork23 from '../components/SemestralWork/SemestralWork23.vue'
import SemestralWork24 from '../components/SemestralWork/SemestralWork24.vue'
import SemestralWork3 from '../components/SemestralWork/SemestralWork3.vue'
import SemestralWork4 from '../components/SemestralWork/SemestralWork4.vue'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'SemestralWork1',
      component: SemestralWork1
    },
    {
      path: '/SW12',
      name: 'SemestralWork12',
      component: SemestralWork12
    },
    {
      path: '/SW13',
      name: 'SemestralWork13',
      component: SemestralWork13
    },
    {
      path: '/SW14',
      name: 'SemestralWork14',
      component: SemestralWork14
    },
    {
      path: '/SW15',
      name: 'SemestralWork15',
      component: SemestralWork15
    },
    {
      path: '/SW2',
      name: 'SemestralWork2',
      component: SemestralWork2
    },
    {
      path: '/SW22',
      name: 'SemestralWork22',
      component: SemestralWork22
    },
    {
      path: '/SW23',
      name: 'SemestralWork23',
      component: SemestralWork23
    },
    {
      path: '/SW24',
      name: 'SemestralWork24',
      component: SemestralWork24
    },
    {
      path: '/SW3',
      name: 'SemestralWork3',
      component: SemestralWork3
    },
    {
      path: '/SW4',
      name: 'SemestralWork4',
      component: SemestralWork4
    }
  ],
  linkActiveClass: 'active'
})
